# Metronome

A simple metronome to meet all your basic metronome needs whilst solely being a metronome. Nice.

View the project live here:  https://cameroncherry.com/webprojects/metronome

(c) 2022 Cameron Cherry - MIT License