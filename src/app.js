const slider = document.getElementById("bpm-slider");
const bpmText = document.getElementById("bpm-number");
const timeSigSelect1 = document.getElementById("time-sig-1");
const timeSigSelect2 = document.getElementById("time-sig-2");
const restartButton = document.getElementById("restartButton");
let bpm = bpmText.value;
let timeSig = [];
let repeatInterval;
const playPauseButton = document.getElementById("playPauseButton");
let soundPlaying = false;
let intervalHandle;
let beatCounter = 0;
let timeoutIDs = []; // for clearing scheduled timeouts
const MIN_BPM = 20;
const MAX_BPM = 280;
const BEATS_IN_CACHE = 1000; // number of beats to load into timeouts
let timeSinceLastKeyPress; // for limiting keypresses to once every second
let timeSinceLastSliderChange; // for limiting slider changes to once every X ms
updateTimeSig();
updateBpmText();

slider.addEventListener("change", onSliderChange);
timeSigSelect1.addEventListener("input", updateTimeSig);
timeSigSelect2.addEventListener("input", updateTimeSig);
restartButton.addEventListener("click", onRestartClick);
bpmText.addEventListener("blur", (e) => {
    e.target.value = Math.max(MIN_BPM, e.target.value);
    onSliderChange();
});
playPauseButton.addEventListener("click", playButtonClick);
window.onkeydown = onKeyboardInput;

function updateRepeatInterval() {
    repeatInterval = 60 / bpm * 1000 * 4 / timeSig[1]; // ms
}

function updateBpmText() {
    bpmText.value = slider.value;
}

function onBpmInput() {
    let x = parseInt(bpmText.value.replace(/[^0-9]/g, ''));
    x = Math.min(MAX_BPM, x);
    if (isNaN(x)) {
        x = MIN_BPM;
    }
    bpmText.value = x;
    moveSliderTo(bpmText.value);
    updatePlaying();
}

function moveSliderTo(x) {
    slider.value = x;
}

function onSliderChange() {
    const currTime = new Date();
    if ( (timeSinceLastSliderChange != null) && (currTime - timeSinceLastSliderChange < 700) ) {
        return;
    }
    timeSinceLastSliderChange = currTime;
    updateBpmText();
    bpm = bpmText.value;
    updatePlaying(restart=true);
}

function updateTimeSig() {
    timeSig[0] = timeSigSelect1.value;
    timeSig[1] = timeSigSelect2.value;
    updatePlaying();
}

function updatePlaying(restart=false) {
    const delayMilliSecs = 500 * restart;
    updateRepeatInterval();
    if (soundPlaying) {
        stopSound();
        playSound(delayMilliSecs);
    }
}

function onRestartClick() {
    beatCounter = 0;
    updatePlaying(restart=true);
}

function onKeyboardInput(e) {
    // Limit keypresses to only once every second as otherwise timeouts can't be cleared fast enough
    const currTime = new Date();
    if ( (timeSinceLastKeyPress != null) && (currTime - timeSinceLastKeyPress < 1000) ) {
        return;
    }
    if (document.activeElement == timeSigSelect1 || document.activeElement == timeSigSelect2) {
        return;
    }
    if (e.code == "Space" && document.activeElement != playPauseButton) {
        playButtonClick();
        timeSinceLastKeyPress = currTime;
        return;
    }
    if (document.activeElement == slider) {
        updateBpmText();
        bpm = bpmText.value;
        timeSinceLastKeyPress = currTime;
        return;
    } else if (e.code == "ArrowLeft" && document.activeElement != bpmText) {
        changeSlider(-1);
        timeSinceLastKeyPress = currTime;
        return;
    } else if (e.code == "ArrowRight" && document.activeElement != bpmText) {
        changeSlider(1);
        timeSinceLastKeyPress = currTime;
        return;
    } else {
        return;
    }
}

function changeSlider(delta) {
    slider.value = parseInt(slider.value) + delta;
    onSliderChange();
}

function playButtonClick() {
    if (bpm !== "") {
        bpm = Math.max(bpm, MIN_BPM);
        updateBpmText();
        updateRepeatInterval();
        if (soundPlaying) {
            playPauseButton.innerHTML = "Play";
            restartButton.classList.toggle("playing");
            restartButton.tabIndex = "-1";
            stopSound();
            soundPlaying = false;
            beatCounter = 0;
        } else {
            soundPlaying = true;
            playPauseButton.innerHTML = "Stop";
            restartButton.classList.toggle("playing");
            restartButton.tabIndex = "0";
            playSound();
        }
    }
}

function sleep(ms) {
    if (ms == 0) return
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function playSound(delayMilliSecs = 0) {
    
    // Sleep if there is a delay scheduled
    await sleep(delayMilliSecs);

    // Initialise audio
    const audioContext = new (window.AudioContext || window.webkitAudioContext)();
    const tick = audioContext.createOscillator();
    const tickVolume = audioContext.createGain();
    tick.type = 'sine'; 
    tick.frequency.value = 1000;
    tickVolume.gain.value = 0;
    tick.connect(tickVolume);
    tickVolume.connect(audioContext.destination);
    
    tick.start(0);

    const timeoutDuration = repeatInterval / 1000;
    let now = audioContext.currentTime;

    // Schedule all future ticks
    for (let i = 0; i < BEATS_IN_CACHE; i++) {
        const x = now;
        clickAtTime(x, tickVolume);
        timeoutIDs.push(setTimeout(function(){clickAtTime(x, tickVolume);}, x * 1000));
        now += timeoutDuration;
    }
};

function clickAtTime(time, tickVolume) {
    beatCounter += 1;
    const firstBeat = (beatCounter % timeSig[0] == 1);
    const vol = 1 + 0.6 * firstBeat;

    // Silence current click sound
    tickVolume.gain.cancelScheduledValues(time);
    tickVolume.gain.setValueAtTime(0, time);

    // Audible click sound 'immediately'
    tickVolume.gain.linearRampToValueAtTime(vol, time + .001);
    tickVolume.gain.linearRampToValueAtTime(0, time + .001 + .01);

}

function stopSound() {
    // Clear all active timeouts set up by metronome
    if (timeoutIDs.length > 0) {   
        for (let i=0; i < BEATS_IN_CACHE; i++) {
            clearTimeout(timeoutIDs[i]);
        }
    }
    timeoutIDs = [];
}